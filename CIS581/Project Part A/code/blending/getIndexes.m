function indexes = getIndexes(mask, targetH, targetW, offsetX, offsetY)

%TODO: i think the resizing might destroy the indexes sequence 1,2,3 to
%1,...,3?
    
% (INPUT) mask: h  w logical matrix representing the replacement region.
% (INPUT) targetH: target image height, h0.
% (INPUT) targetW: target image width, w0.
% (INPUT) offsetX: the x axis o set of source image regard of target image.
% (INPUT) offsetY: the y axis o set of source image regard of target image.
% (OUTPUT) indexes: h0  w0 matrix representing the indices of each replacement
% pixel. 0 means not a replacement pixel.


    % create indices from mask
    % idx_src = find(mask); % indices go downwards!
    indexes = double(mask);
    % indexes(idx_src) = (1:length(idx_src))';
    
%     [y_mask, x_mask] = find(mask);
%     offset_mask_y = min(y_mask);
%     offset_mask_x = min(x_mask);
%     corner_mask_y = max(y_mask);
%     corner_mask_x = max(x_mask);
%     
%     indexes = double(mask(offset_mask_y:corner_mask_y, offset_mask_x:corner_mask_x));
%     
    % require integer values
    offsetX = round(offsetX);
    offsetY = round(offsetY);
    % zero pad by offset
    indexes = padarray(indexes,[offsetY, offsetX],0,'pre');
    % resize
    if size(indexes,1) < targetH
        % post pad zeros to match size
        indexes = padarray(indexes,[targetH - size(indexes,1),0],0,'post');
    else
        % trim
        indexes = indexes(1:targetH,:);
    end
    
    if size(indexes,2) < targetW
        indexes = padarray(indexes,[0, targetW - size(indexes,2)],0,'post');
    else
        % trim
        indexes = indexes(:, 1:targetW);
    end
    
    idx_src = find(indexes);
    indexes(idx_src) = (1:length(idx_src))';
    
end
