function solVector = getSolutionVect(indexes, source, target, offsetX, offsetY)
% (INPUT) indexes: h0w0 matrix representing the indices of each replacement pixel.
% (INPUT) source: h  w matrix representing one channel of source image.
% (INPUT) target: h0  w0 matrix representing one channel of target image.
% (INPUT) offsetX: the x axis o set of source image regard of target image.
% (INPUT) offsetY: the y axis o set of source image regard of target image.
% (OUTPUT) solVector: 1  n vector representing the solution vector.

    n = nnz(indexes);
    solVector = zeros(1,n);
    
    for p = 1:n
        
        Np=4; %default, Np=Np-1 if out of bound
        
        neighbors = [0,0,0,0];
        [yp,xp] = find(indexes == p);
        yp_src = yp - offsetY;
        xp_src = xp - offsetX;
        gp = source(yp_src, xp_src);
        gq = [0,0,0,0];
        fq = [0,0,0,0]; %handle edges: check if index is zero
        % TODO handle out of bounds
        if xp ~= 1
            neighbors(1) = indexes(yp,xp-1);
            if xp_src ~= 1
                gq(1) = source(yp_src, xp_src - 1); %get gq also on edge
            end
                
            if (neighbors(1) == 0)
                fq(1) = target(yp,xp-1);
            end
        else
            Np=Np-1;
        end
        if yp ~= 1
            neighbors(2) = indexes(yp - 1,xp);
            if yp_src ~= 1
                gq(2) = source(yp_src - 1, xp_src);
            end
            if (neighbors(2) == 0)
                fq(2) = target(yp - 1,xp);
            end
        else
            Np=Np-1;
        end
        if xp ~= size(indexes,2)
            neighbors(3) = indexes(yp,xp + 1);
            if xp_src ~= size(source,2)
                gq(3) = source(yp_src, xp_src + 1);
            end
            if (neighbors(3) == 0)
                fq(3) = target(yp,xp + 1);                
            end
        else
            Np=Np-1;
        end
        if yp ~= size(indexes,1)
            neighbors(4) = indexes(yp + 1,xp);
            if yp_src ~= size(source,1)
                gq(4) = source(yp_src + 1, xp_src);
            end
            if (neighbors(4) == 0)
                fq(4) = target(yp + 1,xp);                                
            end
        else
            Np=Np-1;
        end
        
        %Np = nnz(neighbors);

        solVector(p) = Np*double(gp) - double(sum(gq)) + double(sum(fq));
        
    end
end