targetImg = imread('testsun.jpg');
sourceImg = imread('testbird.jpg');

try
    load('image_mask.mat');
catch E
    mask = maskImage(sourceImg);
    save('image_mask.mat', 'mask');
end


% resize source image
[y_mask, x_mask] = find(mask);
src_img_y = min(y_mask) - 1:max(y_mask)+1;
src_img_x = min(x_mask) - 1:max(x_mask)+1;

sourceImg = sourceImg(src_img_y, src_img_x, :);
mask = mask(src_img_y, src_img_x);

figure(1)
% select offset
title('Select offset point for source image.');
imshow(targetImg);
[offsetX, offsetY] = ginput(1);
offsetX = round(offsetX);
offsetY = round(offsetY);
close;

resultImg = seamlessCloningPoisson(sourceImg, targetImg, mask, offsetX, offsetY);

