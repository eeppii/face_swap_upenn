function [Iy, E] = rmHorSeam(I, My, Tby)
% I is the image. Note that I could be color or grayscale image.
% My is the cumulative minimum energy map along horizontal direction.
% Tby is the backtrack table along horizontal direction.
% Iy is the image removed one row.
% E is the cost of seam removal

[nr, nc, nz] = size(I);
rmIdx = zeros(1, nc);
Iy = uint8(zeros(nr-1, nc, nz));

[cost,Idx]=min(My(:,end));
Idx=Idx+nr*(nc-1);%get Idx for size(I)
E=cost;
rmIdx(1,nc)=Idx;

for i=1:nc-1
    Idx=Idx-nr-2+Tby(Idx);%get the next index in the col to the left
    rmIdx(1,nc-i)=Idx;%bookkeeping
end

%remove the seam in all layers (rmIdx is for first layer)
for i=1:nz
    I_temp=I(:,:,i);
    
%     %visualize prep
%     if i==1
%     I_temp(rmIdx)=255;
%     I_viz(:,:,i)=I_temp;
%     else
%     I_temp(rmIdx)=0;
%     I_viz(:,:,i)=I_temp;
%     end
    
    %remove
    I_temp(rmIdx)=[];
    I_temp=reshape(I_temp,[nr-1, nc]);
    Iy(:,:,i)=I_temp;
end

% %visualize image
% figure
% imshow(I_viz);
% assignin('base','I_viz_hor',I_viz);

end
    

