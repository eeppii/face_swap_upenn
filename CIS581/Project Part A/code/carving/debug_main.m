%TODO visualize seam as give in PDF
%TODO speed up process:
%   -> y transpose and then run x fct.
%   -> for T do not loop but use recursion instead
tic
close all;

% I=imread('SequoiaNatPark.jpg');
I=imread('SF.jpg');
figure
imshow(I)

% % tic
% % e = genEngMap(I);
% % toc
% % 
% % tic
% % [My, Tby] = cumMinEngHor(e);
% % % e(end-4:end,end-4:end)
% % % Tby(end-4:end,end-4:end)
% % % My(end-4:end,end-4:end)
% % toc
% % 
% % tic
% % [Mx, Tbx] = cumMinEngVer(e);
% % % e(end-4:end,end-4:end)
% % % Tbx(end-4:end,end-4:end)
% % % Mx(end-4:end,end-4:end)
% % toc
% % 
% % tic
% % [Iy, Ey] = rmHorSeam(I, My, Tby);
% % toc
% % 
% % figure
% % imshow(Iy)
% % 
% % tic
% % [Ix, Ex] = rmVerSeam(I, Mx, Tbx);
% % toc
% % 
% % figure
% % imshow(Ix)

remove_rows=10;
remove_cols=10;
[Ic, T] = carv(I, remove_rows, remove_cols);
figure
imshow(Ic);
toc

% imwrite(I_viz_hor,'I_viz_hor.jpg');
% imwrite(I_viz_hor,'I_viz_ver.jpg');

