function [Ix, E] = rmVerSeam(I, Mx, Tbx)
% I is the image. Note that I could be color or grayscale image.
% Mx is the cumulative minimum energy map along vertical direction.
% Tbx is the backtrack table along vertical direction.
% Ix is the image removed one column.
% E is the cost of seam removal

[nr, nc, nz] = size(I);
rmIdx = zeros(nr, 1);
Ix = uint8(zeros(nr, nc-1, nz));

[cost,Idx]=min(Mx(end,:));
Idx=Idx*nr; %get Idx for size(I)
E=cost;
rmIdx(nr,1)=Idx;

for i=1:nr-1
    
    [row,col] = ind2sub([nr,nc],Idx);
    row=row-1; col=col-2+Tbx(Idx);
    Idx=sub2ind([nr,nc],row,col); %get the next index in the row upwards
    rmIdx(nr-i,1)=Idx;%bookkeeping
end

%remove the seam an all layers (rmIdx is for first layer)
for i=1:nz
    I_temp=I(:,:,i);
    
%     %visualize prep
%     if i==1
%     I_temp(rmIdx)=255;
%     I_viz(:,:,i)=I_temp;
%     else
%     I_temp(rmIdx)=0;
%     I_viz(:,:,i)=I_temp;
%     end
    
    %remove
    [row,col]=ind2sub([nr,nc],rmIdx);
    transpIdx=sub2ind([nc,nr],col,row);
    I_transp=I_temp';
    I_transp(transpIdx)=[];
    I_transp=reshape(I_transp,[nc-1, nr]);
    I_temp=I_transp';
    Ix(:,:,i)=I_temp;
end

% %visualize image
% figure
% imshow(I_viz);
% assignin('base','I_viz_ver',I_viz);

end