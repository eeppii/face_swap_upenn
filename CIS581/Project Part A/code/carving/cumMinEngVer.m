function [Mx, Tbx] = cumMinEngVer(e)
% e is the energy map.
% Mx is the cumulative minimum energy map along vertical direction.
% Tbx is the backtrack table along vertical direction.

[nr,nc] = size(e);
Mx = zeros(nr, nc);
Tbx = zeros(nr, nc);
Mx(1,:) = e(1,:);

%add col on top and botton with highest possible value, never taken as min
e_max=max(e(:))*nr;
e_big=[e_max*ones(nr,1) e e_max*ones(nr,1)];%add max-frame

for i=1:nr-1
   
    %extract 'left' 'mid' and 'right' candidates
    e_right=e_big(i,3:end);
    e_mid=e_big(i,2:end-1);
    e_left=e_big(i,1:end-2);
    %search  min -> index 1=left, 2=mid, 3=right
    [value,index]=min([e_left;e_mid;e_right],[],1);%row-vecs
    e_big(i+1,2:end-1)=e_big(i+1,2:end-1)+value;%don't use max-frame
    Tbx(i+1,:)=index;
    
end

Mx(2:end,:)=e_big(2:end,2:end-1);

end