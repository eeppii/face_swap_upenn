
% tic
close all;

% Isq=imread('SequoiaNatPark.jpg');
% I=imread('SF.jpg');
Isq=imread('thailand_beach.jpg');
I=imread('thailand_boat.jpg');
% figure
% imshow(I)

remove_rows=10;
remove_cols=10;
[Ic, T] = carv(I, remove_rows, remove_cols);
figure
imshow(Ic);
% toc

% imwrite(I_viz_hor,'I_viz_hor.png');
% imwrite(I_viz_ver,'I_viz_ver.png');


% imwrite(Ic,'boat_10_final.png');
% disp('past 1/6')
% 
% remove_rows=10;
% remove_cols=10;
% [Ic, T] = carv(Isq, remove_rows, remove_cols);
% imwrite(Ic,'beach_10_final.png');
% disp('past 2/6')
% 
% remove_rows=20;
% remove_cols=50;
% [Ic, T] = carv(I, remove_rows, remove_cols);
% imwrite(Ic,'boat_2050_final.png');
% disp('past 3/6')
% 
% remove_rows=20;
% remove_cols=50;
% [Ic, T] = carv(Isq, remove_rows, remove_cols);
% imwrite(Ic,'beach_2050_final.png');
% disp('past 4/6')
% 
% remove_rows=50;
% remove_cols=100;
% [Ic, T] = carv(I, remove_rows, remove_cols);
% imwrite(Ic,'boat_50100_final.png');
% disp('past 5/6')
% 
% remove_rows=50;
% remove_cols=100;
% [Ic, T] = carv(Isq, remove_rows, remove_cols);
% imwrite(Ic,'beach_50100_final.png');
% disp('past 6/6')

% %too big
% remove_rows=200;
% remove_cols=400;
% [Ic, T] = carv(I, remove_rows, remove_cols);
% imwrite(Ic,'boat_200400_final.png');
% 
% 
% remove_rows=200;
% remove_cols=400;
% [Ic, T] = carv(Isq, remove_rows, remove_cols);
% imwrite(Ic,'boat_200400_final.png');
