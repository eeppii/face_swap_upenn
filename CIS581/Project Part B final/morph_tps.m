function morphed_im = morph_tps(im1, a1_x, ax_x, ay_x, w_x, a1_y, ax_y, ay_y, w_y, intermed_pts, sz)
% (INPUT) im source: Hs Ws  3 matrix representing the source image.
% (INPUT) a1 x, ax x, ay x, w x: the parameters solved when doing est tps
% in the x direction.
% (INPUT) a1 y, ax y, ay y, w y: the parameters solved when doing est tps
% in the y direction.
% (INPUT) ctr pts: N  2 matrix, each row representing corresponding point
% position (x; y) in target image.
% (INPUT) sz: 1  2 vector representing the target image size (Ht;Wt).
% (OUTPUT) morphed im: Ht Wt  3 matrix representing the morphed image.

[X,Y]=meshgrid(1:sz(2),1:sz(1));
[nr,nc,~]=size(im1);
morphed_im=zeros(sz(1),sz(2),3);
morphed_im=uint8(morphed_im);

%call outsourced function
fx1 = f_from_coeff(X,Y,intermed_pts, a1_x, ax_x, ay_x, w_x );

%do the same for y-coords
fy1 = f_from_coeff(X,Y,intermed_pts, a1_y, ax_y, ay_y, w_y );

%check for image size
[fy] = check_size(fy1,nr);
[fx] = check_size(fx1,nc);

%assess image
morphed_im(:,:,1)=im2uint8(interp2(im2double(im1(:,:,1)),fx,fy));
morphed_im(:,:,2)=im2uint8(interp2(im2double(im1(:,:,2)),fx,fy));
morphed_im(:,:,3)=im2uint8(interp2(im2double(im1(:,:,3)),fx,fy));

end

