function [My, Tby] = cumMinEngHor(e)
% e is the energy map.
% My is the cumulative minimum energy map along horizontal direction.
% Tby is the backtrack table along horizontal direction.

[nr,nc] = size(e);
My = zeros(nr, nc);
Tby = zeros(nr, nc);
My(:,1) = e(:,1);

%add row on top and botton with highest possible value, never taken as min
e_max=max(e(:))*nc;
e_big=[e_max*ones(1,nc);e;e_max*ones(1,nc)];%add max-frame

for i=1:nc-1
   
    %extract 'down' 'mid' and 'up' candidates
    e_down=e_big(3:end,i);
    e_mid=e_big(2:end-1,i);
    e_up=e_big(1:end-2,i);
    %search  min -> index 1=up, 2=mid, 3=down
    [value,index]=min([e_up';e_mid';e_down'],[],1);%row-vecs
    e_big(2:end-1,i+1)=e_big(2:end-1,i+1)+value';%don't use max-frame
    Tby(:,i+1)=index';
    
end

My(:,2:end)=e_big(2:end-1,2:end); 

end