function [ resultImg ] = blend_replace_face( bbox, mask, sourceImg,targetImg )
%blend the pixels of the replacement and target face

addpath blending_functions

position_bbox=[bbox(1),bbox(2)]; %[x,y]

% resize source image
[y_mask, x_mask] = find(mask);
mask_offset_y=min(y_mask) - 1;
src_img_y = mask_offset_y:max(y_mask)+1;
mask_offset_x=min(x_mask) - 1;
src_img_x = mask_offset_x:max(x_mask)+1;

sourceImg = sourceImg(src_img_y, src_img_x, :);
mask = mask(src_img_y, src_img_x);

% select offset
offsetX = position_bbox(1)+mask_offset_x-1;
offsetY = position_bbox(2)+mask_offset_y-1;

resultImg = seamlessCloningPoisson(sourceImg, targetImg, mask, offsetX, offsetY);

end

