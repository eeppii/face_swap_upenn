function [ CostMaps ] = extract_cost_maps( Codebook, Img )
%EXTRACT_COST_MAP Get cost maps from shape context features of codebook
% (INPUT)
%   CodeBook cell array (num_faces, num_objects)
%       each entry is a cell of {shape_context_feature, distance to nose}
%   Img (H x W) grayscale image of face
% (OUTPUT)
%   CostMaps (num_objects, 1) cell matrix of the cost maps for each object
    % ordering left eye, right eye, nose, mouth
    % each map is a matrix of the size of the image
    
    num_objects = size(Codebook,2);
    num_faces = size(Codebook,1);
    
    try 
        Img = rgb2gray(Img);
    catch
    end
    
    Img = double(Img);
    
    % Compute Harris features
    %cimg = corner_detector(Img);
    [Gmag, Gdir] = imgradient(Img);
    BW = edge(Img, 'canny');
    % apply mask at edges
    cimg = Gmag.*BW;
    % % Take top edge features
    [x_feat, y_feat, ~] = anms(cimg, 600);
    E_orientations = 2*pi*Gdir / 180;
    
    % height and width
    [H, W] = size(Img);
    
    CostMaps = cell(num_objects, 1);
    
    for i = 1:num_objects
        CostMaps{i} = Inf(H,W);
    end

    % SLIDING WINDOW GOING PIXEL BY PIXEL
    for y = 1:4:H  
        for x = 1:4:W
            for obj = 1:num_objects
                % Take minimum distance and output heat map
                cost = Inf;
                for face = 1:4
                    window_size = Codebook{face, obj}{1,2};
                    SC = extract_shape_context_features(window_size, x, y, x_feat, y_feat, E_orientations);
                    % check for non-zero shape context feature
                    if any(SC)
                        dist = chi_square_dist(SC, Codebook{face,obj}{1,1});
                        if (dist < cost)
                           cost = dist;
                        end
                    end
                end
                CostMaps{obj}(y,x) = cost;
            end
        end
        
    end


end

