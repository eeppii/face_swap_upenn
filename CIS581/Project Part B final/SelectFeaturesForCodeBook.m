%script loads movie sequence and asks user to select manually face features
%script stores features in face_cell and their location in face_loc_cell
%as [x1,y1,width,height]

%face_cell is (nfaces,nfeatures+1) -> first entry is whole face
%face_loc_cell is (nfaces,nfeatures+1) -> first entry is size of whole face

close all;

% Load video
videoFileReader = vision.VideoFileReader('Proj4_Test_Videos/easy/easy4.mp4');
faceDetector = vision.CascadeObjectDetector();
EOF = false;

%number of features-> 2eye,1nose,1mouth
num_feat = 4;

%get 10-20 replacement faces
nfaces = 1;
%create cell for all faces (row) and original face (col1) all features(cols 2-5)
face_objects = cell(nfaces,num_feat+1);
%crate cell to store location and size of features
%size of face in 1, loc+size features 2-5
face_locations = cell(nfaces,num_feat+1);


% extract each frame
i=0;
while ~EOF
    %% Detect faces in video frame
    [videoFrame, EOF] = step(videoFileReader);
    figure(1)
    imshow(videoFrame); title('Do you want to use that face?');
    movegui(figure(1),'east')
    user_entry = input('Do you want to use a face of this frame?->1=yes, 0=no');
    if user_entry
        i = i+1;
        bbox = step(faceDetector, videoFrame);
        %bbox format: upper left corner + width + height
        videoFrame_rec = insertShape(videoFrame, 'Rectangle', bbox);
        figure(1)
        imshow(videoFrame_rec); title('Detected faces - Please select upper-left and lower-right point of the replacement face');
        movegui(figure(1),'east')
        [x1,y1] = ginput(1);
        [x2,y2] = ginput(1);
        J = videoFrame(y1:y2,x1:x2,:);
        face_objects{i,1} = J;
        face_locations{i,1} = size(J);
        figure(1)
        imshow(J);
        % movegui(figure(1),'east')
        for j=1:num_feat
            %figure(1)
            %imshow(J);
            %movegui(figure(1),'east')
            figure(1);
            title(['Please select middle and any corner point for the ',num2str(j),'th feature in the ',num2str(i),'th face']);
            [x1,y1] = ginput(1);
            [x2,y2] = ginput(1);
            deltax=abs(x2-x1);
            deltay=abs(y2-y1);
            JJ = J(y1-deltay:y1+deltay,x1-deltax:x1+deltax,:);
            face_locations{i,j+1} = [x1-deltax,y1-deltay,2*deltax,2*deltay]; %IMPORTANT: convention to store face features in entry 2-5 of face_loc_cell
            figure(2)
            imshow(JJ);
            movegui(figure(2),'north')
            title('selected feature')
            face_objects{i,j+1} = JJ;
            figure(3)
            imshow(J);
            hold on
            plot(x1-deltax,y1-deltay,'r*');
            plot(x1,y1,'b*');
            
        end
        
    end
    
save('test_face_objects_squares3','face_objects');
save('test_face_locations_squares3','face_locations');
    
end

