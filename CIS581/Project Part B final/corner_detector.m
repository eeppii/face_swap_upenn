% img is an image
% cimg is a corner matrix

function [cimg] = corner_detector(img)
% (INPUT) img: H � W matrix representing the gray scale input image.
% (OUTPUT) cimg: H � W matrix representing the corner metric matrix.
    
    try
        img = rgb2gray(img);
    catch
    end
    
    % Function from Computer Vision Toolbox
    c_points = detectHarrisFeatures(img);
    
    H = size(img,1);
    W = size(img,2);
    cimg = zeros(size(img));
    
    % X, Y locations
    Locations = round(c_points.Location);
    % Fix out of bounds
    Locations(Locations < 1) = 1;
    Locations(Locations(:,1) > W, 1) = W;
    Locations(Locations(:,2) > H, 2) = H;
    %Locations = [Locations(:,2), Locations(:,1)];

    idx = sub2ind(size(img), Locations(:,2), Locations(:,1));
    cimg(idx) = c_points.Metric;

end