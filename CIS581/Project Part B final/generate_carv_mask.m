function [mask]=generate_carv_mask(warped_face,P)
%input: warped_face nr x nc
%output: carving mask of warped face, of size nr x nc and mask has =1
%entries, non-mask areas are =0
% P=[xeyel,yeyel;xeyer,yeyer;xnose,ynose;xmouth,ymouth]

% addpath carv_functions
% addpath logsample
% addpath warping_functions

e = genEngMap(warped_face);
%track feature location so that carving wont cut out those
hack_XY=[P(:,1),P(:,2)];
hack_XY(3,:)=[]; %nose not needed, will later be center anyway
[nr_e,nc_e]=size(e);
%image corner are needed for later finding the offset
hack_XY=[[1 1 nc_e nc_e]',[1 nr_e 1 nr_e]';hack_XY];
%note: for simplicyty hack_XY contains in first 4 rows the corner and in
%later 3 rows the feature locations


rmin=1;
rmax=max(size(e));
nr=rmax;
middle=P(3,:);
xc=middle(1);
yc=middle(2);

%go to logpolar space
[logarr,W_R] = logsample(e, rmin, rmax, xc, yc, nr, [],hack_XY);
e_logpolar=logarr';

e_logpolar(e_logpolar==0)=max(e_logpolar(:)*2); %out of bound pixel have zero value, therefore e==0 too,
%as those pixel must not be used for carving, set to high e-value.

[My, Tby] = cumMinEngHor(e_logpolar);
[Idx_polar] = rmHorSeam(e_logpolar, My, Tby,W_R(end-2:end,:));
[R_carv,W_carv]=ind2sub(size(e_logpolar),Idx_polar);
% hack(Idx_polar)=3;

% logarr_cart= logsampback(logarr, rmin, rmax);
[~,XY_carv,XY_imgcorn]=logsampback(logarr, rmin, rmax,[W_carv(:),R_carv(:)],W_R(1:4,:));
offsetY=XY_imgcorn(1,2);
offsetX=XY_imgcorn(1,1);
X_carv=XY_carv(:,1)-offsetX;
Y_carv=XY_carv(:,2)-offsetY;
[X_carv] = check_size(X_carv,nc_e);
[Y_carv] = check_size(Y_carv,nr_e);

mask = poly2mask(X_carv, Y_carv, nr_e, nc_e);

%outher most pixels are not allowed to be in mask, as needed by blending
%fct.
mask(1,:)=0;
mask(end,:)=0;
mask(:,1)=0;
mask(:,end)=0;
% %viz
% figure
% imagesc(mask);

end


