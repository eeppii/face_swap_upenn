function [a] = check_size(a,n)
%check if in image

a(a<1)=1;
a(a>n)=n;

end
