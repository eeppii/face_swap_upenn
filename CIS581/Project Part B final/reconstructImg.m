function resultImg = reconstructImg(indexes, red, green, blue, targetImg)
% (INPUT) indexes: h0w0 matrix representing the indices of each replacement pixel.
% (INPUT) red: 1  n vector representing the red channel intensity of replacement
% pixel.
% (INPUT) green: 1n vector representing the green channel intensity of replacement
% pixel.
% (INPUT) blue: 1  n vector representing the blue channel intensity of replacement
% pixel.
% (INPUT) targetImg: h0  w0  3 matrix representing target image.
% (OUTPUT) resultImg: h0  w0  3 matrix representing blending image.

[h0,w0]=size(indexes);

resultImg=targetImg;
% resultImg=uint8(resultImg); %dont need that currently 

fill_in = find(indexes);
resultImg(fill_in) = red;
resultImg(fill_in+h0*w0) = green;
resultImg(fill_in+2*h0*w0) = blue;


end