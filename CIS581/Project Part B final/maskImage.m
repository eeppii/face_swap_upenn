function mask = maskImage(img)
    % (INPUT) img: h � w � 3 matrix representing the source image.
    % (OUTPUT) mask: h � w logical matrix representing the replacement region.
    imshow(img);
    title('Click and drag around desired shape');
    h = imfreehand;
    mask = createMask(h);
end

