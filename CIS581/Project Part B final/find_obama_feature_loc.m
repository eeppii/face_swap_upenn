function [obama_feat_loc] = find_obama_feature_loc(image)
% image=imread('obama_face.png');
figure(1)
imshow(image);
obama_feat_loc=zeros(4,2);
for i=1:4
[x1,y1] = ginput(1);
[x2,y2] = ginput(1);
obama_feat_loc(i,1)=x1+(x2-x1)/2;
obama_feat_loc(i,2)=y1+(y2-y1)/2;
end
% save('obama_feat_loc','obama_feat_loc')

%notation in obama_feat_loc: [xeyel yeyel;
%                            xeyer yeyer;
%                            xnose ynose;
%                            xmouth ymout]