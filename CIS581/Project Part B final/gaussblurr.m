function  [conII]=gaussblurr(test_feature,size,sigma)

 Gx = normpdf(-size:1:size, 0, sigma); %TODO, is it ok size_filter=5 and sigma=number of subsamples?
 Gy = Gx';
 
 big_test_feature=padarray(test_feature,[size size],'symmetric','both');
 conI=conv2(double(big_test_feature),Gx,'valid');
 conII=conv2(conI,Gy,'valid');


end

