function [ d ] = l2_norm( V )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    % l2 norm of each row
    d = sqrt(sum(V.^2, 2));
end

