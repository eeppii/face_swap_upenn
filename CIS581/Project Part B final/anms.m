function [x, y, rmax] = anms( cimg, max_pts )
% Adaptive Non-Maximal Suppression
% cimg = corner strength map
% max_pts = number of corners desired
% [x, y] = coordinates of corners
% rmax = suppression radius used to get max_pts corners
%
%   (INPUT) cimg: H � W matrix representing the corner metric matrix. 
%   (INPUT) max pts: the number of corners desired.
%   (OUTPUT) x: N � 1 vector representing the column coordinates of corners. 
%   (OUTPUT) y: N � 1 vector representing the row coordinates of corners. 
%   (OUTPUT) rmax: suppression radius used to get max pts corners.
    l2_norm = @(x) sqrt(sum(x.^2,2));
    
    % retrieve non zero element indices
    idx = find(cimg);
    [y_c,x_c] = find(cimg);
    % non-zero corners
    c_points = cimg(idx);
    
    % initialize maximum radius vector
    N = length(idx);
    max_radius = zeros(N,1);
    
    if (N == 0)
        x = [];
        y = [];
        rmax = 0;
        return
    end
    
    % Sort corners
    [~, sorted_idx] = sort(c_points, 'descend');
    y_c = y_c(sorted_idx);
    x_c = x_c(sorted_idx);
    
    % Largest magnitude corner is always considered
    max_radius(1) = max(size(cimg)) + 1;
    
    % for every corner
    for i = 2:N
        % Find distance to closest point that is larger than current point
        % i.e. maximum radius current point could be considered a corner
        max_radius(i) = min(l2_norm(bsxfun(@minus, [x_c(1:i-1), y_c(1:i-1)], [x_c(i),y_c(i)])));
    end
    
    % Sort maximum radii
    [~, idx_r] = sort(max_radius, 'descend');
    
    % Take only up to max_pts
    if length(idx_r) > max_pts
        idx_r = idx_r(1:max_pts);
    end
    
    % Retrieve x and y coordinates
    x = x_c(idx_r);
    y = y_c(idx_r);
    rmax = max_radius(1);

end

