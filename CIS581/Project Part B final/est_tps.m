function [a1,ax,ay,w] = est_tps(intermed_pts, im_x)
% (INPUT) ctr pts: N  2 matrix, each row representing corresponding point
% position (x; y) in second image.
% (INPUT) target value: N1 vector representing corresponding point position
% x or y in rst image
% (OUTPUT) a1: double, TPS parameters.
% (OUTPUT) ax: double, TPS parameters.
% (OUTPUT) ay: double, TPS parameters.
% (OUTPUT) w: N  1 vector, TPS parameters.

%Xi is mesh with const on rows
%Xj is mesh with const on cols
[Xj,Xi]=meshgrid(intermed_pts(:,1),intermed_pts(:,1));
[Yj,Yi]=meshgrid(intermed_pts(:,2),intermed_pts(:,2));

p=length(intermed_pts(:,1));
r=sqrt((Xi-Xj).^2+(Yi-Yj).^2);
%to prevent high (neg) values in log
r(r<1e-9)=0;
K=r.^2.*log(r.^2); %this will throw NaN for r==0
K((isnan(K) == 1))=0;
v=[im_x;zeros(3,1)];
P=[intermed_pts ones(p,1)];
lambda=1e-9;
coeff=inv([K P;P' zeros(3)]+lambda*eye(p+3))*v;
w=coeff(1:p);
ax=coeff(p+1);
ay=coeff(p+2);
a1=coeff(p+3);
end

