function [Ic, T] = carv(I, nrr, ncr)
% I is the image being resized
% [nrr, ncr] is the numbers of rows and columns to remove.
% Ic is the resized image
% T is the transport map

T = zeros(nrr+1, ncr+1);
TI = cell(nrr+1, ncr+1);
TI{1,1} = I;
% TI is a trace table for images. TI{r+1,c+1} records the image removed r rows and c columns.

%calc all T members on first col
for i=2:nrr+1
    %remove seam in hor
    e = genEngMap(TI{i-1,1});
    [My, Tby] = cumMinEngHor(e);
    [Iy, Ey] = rmHorSeam(TI{i-1,1}, My, Tby);
    T(i,1)=T(i-1,1)+Ey;
    TI{i,1}=Iy;
end
%calc all T members on first row
for j=2:ncr+1
    %remove seam in vert
    e = genEngMap(TI{1,j-1});
    [Mx, Tbx] = cumMinEngVer(e);
    [Ix, Ex] = rmVerSeam(TI{1,j-1}, Mx, Tbx);
    T(1,j)=T(1,j-1)+Ex;
    TI{1,j}=Ix;
end
%calc rest of T, all cols for each row
for i=2:nrr+1
   for j=2:ncr+1
    %remove seam in hor
    e_hor = genEngMap(TI{i-1,j});
    [My, Tby] = cumMinEngHor(e_hor);
    [Iy, Ey] = rmHorSeam(TI{i-1,j}, My, Tby);
    %remove seam in vert
    e_vert = genEngMap(TI{i,j-1});
    [Mx, Tbx] = cumMinEngVer(e_vert);
    [Ix, Ex] = rmVerSeam(TI{i,j-1}, Mx, Tbx);
    %compare
    if Ey<=Ex
        T(i,j)=T(i-1,j)+Ey;
        TI{i,j}=Iy;
    else
        T(i,j)=T(i,j-1)+Ex;
        TI{i,j}=Ix;
    end%end if else
        
   end%end j
end%end i

Ic = TI{nrr+1,ncr+1};

end