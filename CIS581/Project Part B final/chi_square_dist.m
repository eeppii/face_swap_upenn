function [ D ] = chi_square_dist( X, y )
%CHI_SQUARE_DIST Computes the chi square distance
%   X is a vector or matrix of size (n, m)
%   y is a vector of size (n,1)
    D = (bsxfun(@minus,X,y).^2)./(bsxfun(@plus,X,y));
%     [x,y] = find(isnan(D));
%     D(x,y) = 0;
    D(isnan(D))=0;
	D = (1/2)*sum(D);

end

