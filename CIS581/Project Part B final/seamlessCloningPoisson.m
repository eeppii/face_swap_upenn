function resultImg = seamlessCloningPoisson(sourceImg, targetImg, mask, offsetX, offsetY)
% (INPUT) sourceImg: h  w  3 matrix representing source image.
% (INPUT) targetImg: h0  w0  3 matrix representing target image.
% (INPUT) mask: h  w logical matrix representing the replacement region.
% (INPUT) offsetX: the x axis o set of source image regard of target image.
% (INPUT) offsetY: the y axis o set of source image regard of target image.
% (OUTPUT) resultImg: h0  w0  3 matrix representing blending image.

disp('past start')
%%
[targetH, targetW, ~] = size(targetImg);

% Index masked pixels
indexes = getIndexes(mask, targetH, targetW, offsetX, offsetY);
disp('past getIndexes')
%%
% Compute A matrix for linear equation
coefM = getCoefMatrix(indexes);
disp('past getCoefMatrix')
%%
% Compute b vector for linear equation
% For each rgb channel
% B_rgb = zeros(3,size(coefM,1));
X_rgb = zeros(3,size(coefM,2));
%%
for i = 1:3
    B_rgb= getSolutionVect(indexes, sourceImg(:,:,i), targetImg(:,:,i), offsetX, offsetY);
    X_rgb(i,:) = (coefM\(B_rgb)')'; 
    disp(['past round',num2str(i)]);
end
disp('past getSolutionVect')
%%
resultImg = reconstructImg(indexes, X_rgb(1,:), X_rgb(2,:), X_rgb(3,:), targetImg); %X_rgb(i,:) is rgb
figure
title('final image')
imshow(resultImg);
disp('done')

end