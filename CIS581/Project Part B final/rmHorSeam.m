function [rmIdx] = rmHorSeam(e_logpolar, My, Tby,W_R)
% I is the image. Note that I could be color or grayscale image.
% My is the cumulative minimum energy map along horizontal direction.
% Tby is the backtrack table along horizontal direction.
% Iy is the image removed one row.
% E is the cost of seam removal
%W_R contains in col1 the corresponding W values, col2 te R values

[nr, nc] = size(e_logpolar);
rmIdx = zeros(1, nc);

[~,Idx_vec]=sort(My(:,end),'ascend');

%get locations of W_R features
max_try=length(Idx_vec);
seams_book=cell(1,1);
test_seams=10;
test_counter=1;

for k=1:max_try
    
    Idx=Idx_vec(k);
    
    Idx=Idx+nr*(nc-1);%get Idx for size(I)
    rmIdx(1,nc)=Idx;

    for i=1:nc-1
        Idx=Idx-nr-2+Tby(Idx);%get the next index in the col to the left
        rmIdx(1,nc-i)=Idx;%bookkeeping
    end
    
    %test if features contained
    [test_R,~]=ind2sub([nr,nc],rmIdx);

    if min(test_R(W_R(:,1))'>W_R(:,2))==1
        seams_book{test_counter,1}=rmIdx; %candidate for seam closing
        test_counter=test_counter+1;
        if test_counter>test_seams %got 10 seams with all 4 features
        break;
        end
    end    
    %if no success at all->take first one
    if k==max_try
        disp('failed to carv 10 times around all 4 features')
        if test_counter==1
            disp('failed to carv even once around all 4 features')
            Idx=Idx_vec(1);
            Idx=Idx+nr*(nc-1);%get Idx for size(I)
            rmIdx(1,nc)=Idx;
            for i=1:nc-1
            Idx=Idx-nr-2+Tby(Idx);%get the next index in the col to the left
            rmIdx(1,nc-i)=Idx;%bookkeeping
            end 
            seams_book{1,1}=rmIdx;
        end
    end
    
end

%rmIdx contains all idx for the least cost seam containing (if possible)
%all 4 features
%now rearange last piece of seam so that seam is nicely closed
num_candidates=length(seams_book);
cost_candidate_save=nc*max(e_logpolar(:));%as high as it can get
best_candidate=seams_book{1,1};
if num_candidates~=1
    for i=1:num_candidates
        cost_candidate=0;

        %get actual and where-it-should-be index 
        rmIdx=seams_book{i,1};
        IdxEnd=rmIdx(end);
        IdxStart=rmIdx(1);
        IdxStartEnd=IdxStart+nr*(nc-1);
        Idx1=IdxEnd-nr*(nc-1);%get Idx for size(I) in first col
        IdxDiff=abs(Idx1-IdxStart); %how much it differs
        if IdxDiff~=0 & IdxDiff~=1

            %go back by IdxDiff and connect nicely
            IdxKnot=rmIdx(end-IdxDiff);
            IdxKnotEnd=IdxKnot+IdxDiff*nr;%get Idx of Knot at end
            rmIdx(end)=IdxStartEnd; %where it needs to go
            IdxMove=(IdxStartEnd-IdxKnotEnd)/IdxDiff;%what needs to be connected
            for j=1:IdxDiff-1%new values in-between
               NewIdx=round(rmIdx(end-IdxDiff+j-1)+nr+IdxMove);%find connection
               rmIdx(end-IdxDiff+j)=NewIdx;
               cost_candidate=cost_candidate+e_logpolar(NewIdx); %get connection cost      
            end
            cost_candidate=cost_candidate+My(IdxKnot)+e_logpolar(IdxStartEnd);%get total cost of new seam
            if cost_candidate<cost_candidate_save %improved?
                best_candidate=rmIdx;
                cost_candidate_save=cost_candidate;
            end
        else
            cost_candidate_save=0;
            best_candidate=seams_book{i,1};       
        end
    end  
end
%take min connection cost, replace in rmIdx
rmIdx=best_candidate;
end
    

