function [ logarr_transp ] = cart_to_logpolar( cart ,target_face_feat_loc)
%input: image in cartesian coordinates
%input: middle=[x,y]
%output: image but in logpolar coordinates: rows -> log(r), cols->theta

rmin=1;
rmax=max(size(cart));
nr=rmax;
middle=target_face_feat_loc(3,:);
xc=middle(1);
yc=middle(2);

logarr = logsample(cart, rmin, rmax, xc, yc, nr, []);

% logarr_onlyfeatures = logsample(target_face_feat_loc, rmin, rmax, xc, yc, nr, []);

logarr_transp=logarr';



end

