function [final_frame]=just_replace_face(bbox,mask, warped_face,videoFrame)
%without blending
[nr,nc,~]=size(warped_face);
[Nr,Nc,~]=size(videoFrame);

position_bbox=[bbox(1),bbox(2)]; %[x,y]

%get infro from warped face
[y_mask, x_mask] = find(mask);
[idx_replace]=sub2ind(size(mask),y_mask,x_mask);

%find loc in videoFrame
y_srcimg=y_mask+position_bbox(2)-1;
x_srcimg=x_mask+position_bbox(1)-1;
[idx_src]=sub2ind(size(videoFrame),y_srcimg,x_srcimg);

videoFrame(idx_src)=warped_face(idx_replace);
videoFrame(idx_src+Nr*Nc)=warped_face(idx_replace+nr*nc);
videoFrame(idx_src+2*Nr*Nc)=warped_face(idx_replace+2*nr*nc);

final_frame=videoFrame;
end