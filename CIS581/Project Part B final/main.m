%% Main script for final face morphing
close all;

%with blending?
blending=1;
%with costmap?
ready=1;
% create video?
make_video=1;

% number of frames in video
num_frames_in_video = 40;

% addpath facesJPG

%load replacement face
obama=imread('obama_face.png');
% load('obama_feat_loc.mat')
try
    load('obama_feat_loc')
catch
    [obama_feat_loc] = find_obama_feature_loc(obama);
end

%%
%load test_faces
% load('test_face_objects_squares2.mat')
% load('test_face_locations_squares2.mat')
load('test_face_objects.mat')
load('test_face_locations.mat')
%load codebook

try
    load('codebook')
    load('codebook_delta')
catch
    [ Codebook, Codebook_delta ] = build_codebook( face_objects, face_locations);

end

%%

% Load video
try
    videoFileReader = vision.VideoFileReader('easy4.mp4');
catch
    videoFileReader = vision.VideoFileReader('Proj4_Test_Videos/easy/easy4.mp4');
end
faceDetector = vision.CascadeObjectDetector();
EOF = false;

% Corresponding points
P = zeros(4,2);

%bookkeeping for final video
bookkeeper=cell(1,5);
book_idx=1;
% extract each frame
frame_num = 1;
while ~EOF && (frame_num < num_frames_in_video)
    %% Detect faces in video frame
    [videoFrame, EOF] = step(videoFileReader);
    bbox = step(faceDetector, videoFrame);
    %bbox format: upper left corner + width + height
%     if isempty(bbox)
%         disp('there was a frame without a face');
%     end

    [nfaces,~]=size(bbox);
    if nfaces>1
%         disp('there was a frame with 2 faces or more - biggest bbox was taken');
        [~,biggest]=max((bbox(:,3)+bbox(:,4))./2); %get biggest square like box
        %face recognition of matlab tends to pick up background noise as
        %face in relatively small bboxes
        bbox = bbox(biggest,:);        
        
    end
    
    %vizualize detected face
    videoFrame_rec = insertShape(videoFrame, 'Rectangle', bbox);
%     figure(1)
%     imshow(videoFrame_rec); title('Detected face');

    % Extract target objects (nose, left eye, right eye, mouth, etc.)
    videoFrame_cut=videoFrame(bbox(2):bbox(2)+bbox(4),bbox(1):bbox(1)+bbox(3),:);
    videoFrame_cut=rgb2gray(videoFrame_cut);
    videoFrame_cut=double(videoFrame_cut);
    %viz
%     imshow(videoFrame_cut)

    if ready
        [ CostMaps ] = extract_cost_maps( Codebook, videoFrame_cut );
        [ P_new ] = extract_corresponding_points( CostMaps,  Codebook_delta);
        if frame_num == 1
            P = P_new;
        end
        if any(l2_norm(bsxfun(@minus, P_new, P_new(3,:)) - Codebook_delta) > 0.4*max(l2_norm(Codebook_delta)))
            % use old P
            disp('Large deformation of corresponding points.');
        else
            % filter positions of corresponding points
            P = 0.7*P_new + 0.3*P;
        end

        %imshow(videoFrame);
        %locations = bsxfun(@plus, P, bbox(1:2) - [1,1]);
        %hold on
        %plot(locations(:,1), locations(:,2), 'r+');
    else
        P = find_obama_feature_loc(videoFrame_cut); %just for debugging
    end
    
    %% Image warp replacement face
    [warped_face]=warp_replacement_face(obama,obama_feat_loc,videoFrame_cut,P);
    %% Carv image
    [mask]=generate_carv_mask(warped_face,P);
    
    %% Replace pixels and blending
    videoFrame=im2uint8(videoFrame);

    %viz
        bookkeeper{book_idx,1}=videoFrame;
        bookkeeper{book_idx,2}=warped_face;
        bookkeeper{book_idx,3}=mask;
        bookkeeper{book_idx,4}=bbox;
        [final_frame]=just_replace_face(bbox,mask, warped_face,videoFrame);
    
    %do bookkeeping
    bookkeeper{book_idx,5}=final_frame;
    book_idx=book_idx+1;
    figure(1)
    title('final frame');
    imshow(final_frame)
    
    frame_num = frame_num + 1;
end
%%
if make_video
    fname='obama_sneaks_in';
    h_avi = VideoWriter(fname, 'Uncompressed AVI');
    frame_rate=10;
    h_avi.FrameRate = frame_rate;
    h_avi.open();
    
    [nframes,~]=size(bookkeeper);
    
    for i=1:nframes
        if blending
        videoFrame=bookkeeper{i,1};
        warped_face=bookkeeper{i,2};
        mask=bookkeeper{i,3};
        bbox=bookkeeper{i,4};
        [final_frame]=blend_replace_face(bbox,mask, warped_face,videoFrame);
        img=final_frame;
        else
        img=bookkeeper{i,5};
        end
    
    imagesc(img);
        if mean(img(:))~=255 %get rid of false blank frames in videowriter
        axis image; axis off;drawnow;
        h_avi.writeVideo(getframe(gcf));
        end
    end
    
    h_avi.close();
    clear h_avi;

end

