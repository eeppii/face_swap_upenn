function fx = f_from_coeff(X,Y,intermed_pts, a1_x, ax_x, ay_x, w_x )
%outsourcing fct

fx=zeros(size(X));
p=length(intermed_pts(:,1));

for i=1:p
   x=intermed_pts(i,1);
   y=intermed_pts(i,2);
   deltax=x-X;
   deltay=y-Y;

   r=sqrt(deltax.^2+deltay.^2);
   %prevent high numbers in log
   r(r<1e-9)=0;
   fx=fx+w_x(i)*(r.^2.*log(r.^2));
end

%set log(0) to zero due to r==0
fx((isnan(fx) == 1))=0;
%complete fx calc.
fx=fx+a1_x+ax_x.*X+ay_x.*Y;

end

