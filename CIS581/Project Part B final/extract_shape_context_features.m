function [ SC ] = extract_shape_context_features( Window_size, x_center, y_center, x_feat, y_feat, E_orientations)
%UNTITLED Summary of this function goes here
% (INPUT)
%   Window_size [height, width] of the window
%   x_center position of window/object center
%   y_center position of window/object center
%   x_feat is an (n x 1) vector of the feature coordinates of the image
%   y_feat is an (n x 1) vector of the feature coordinates of the image
%   E_orientaion is a matrix of edge orientaions of the image size (N, M)
% (OUTPUT)
%   SC is a (feature_length, 1) vector of the shape context feature
    
    n_theta = 12; % number of angle bins
    n_r = 4; % radius bins
    % n_sc = 8; % number of shape contexts (circles)
    
    feature_length = n_r * n_theta;
    
    % Algorithm
    % 1. Run Harris detector to extract corner features
    % 2. Run anms with max SCs (n_sc)
    % 3. Compute average angles on SCs
    % 4. Reset zero for angles as average
    % 5. Bin Angles and radius
    
    % 1. Harris
    % cimg = corner_detector(I);
    % Non-maximal suprression
    % x and y are positions of corners
    % [x, y, rmax] = anms(cimg, n_sc);
    % n = size(x,1);
    window_height = Window_size(1);
    window_width = Window_size(2);
    
    % features within window
    idx_in_window = (x_feat >= x_center - (window_width/2)) & (x_feat <= (x_center + (window_width/2))) & (y_feat >= y_center - (window_height/2)) & (y_feat <= (y_center + (window_height/2)));
    x_feat_window = x_feat(idx_in_window);
    y_feat_window = y_feat(idx_in_window);
    
    
    if isempty(x_feat_window)
        SC = zeros(feature_length,1);
        return
    end
    % radius of shape context
    radius_sc = max(Window_size)/2;
   
    % Get angles of corner features surrounding center of window
    
    angles = mod(atan2(bsxfun(@minus, y_feat_window, y_center)', bsxfun(@minus, x_feat_window, x_center)')',2*pi);
    
    % get edge orientations
    E_orientations = E_orientations(sub2ind(size(E_orientations), y_feat_window, x_feat_window));
    
    % Get log distances of corner features surrounding center of window
    log_distances = log(l2_norm([bsxfun(@minus, x_feat_window, x_center), bsxfun(@minus, y_feat_window, y_center)]));

     % 3. Compute average angle
    avg_angle = mean(angles);

    % 4. Subtract average angle
    % and limit to 0 to 2pi
    E_orientations = mod(E_orientations - avg_angle, 2*pi);
    
    % delta theta for bins
    d_theta = (2*pi/n_theta);
    
    % delta radius
    d_r = log(radius_sc) / n_r;
    
    % radius overlap for blurring
    r_overlap = d_r / 12;
    % angle blurring
    theta_overlap = d_theta / 6;
    
    % 5. compute historgram
    histogram = zeros(feature_length, 1);
    
    %loop
    for th_i = 1:n_theta
        for radius_i = 1:n_r
            log_radius = radius_i * d_r;
            theta = th_i * d_theta;
            theta_overlap = radius_i * d_theta / 6;
            % compute histogram with blurring on edges
            if th_i == 1
                histogram((th_i - 1)*n_r + radius_i) = sum( (((log_radius - d_r - r_overlap) <= log_distances) & (log_distances < log_radius + r_overlap)) ...
                & (((0 <= E_orientations) & (E_orientations < theta + theta_overlap)) | (((2*pi - theta_overlap)<=E_orientations) & E_orientations<(2*pi))));
            elseif th_i == n_theta
                histogram((th_i - 1)*n_r + radius_i) = sum( (((log_radius - d_r - r_overlap) <= log_distances) & (log_distances < log_radius + r_overlap))...
                & ((((theta - d_theta - theta_overlap) <= E_orientations) & (E_orientations < theta)) | ((0<=E_orientations) & (E_orientations<theta_overlap))));                                       

            else
                histogram((th_i - 1)*n_r + radius_i) = sum( (((log_radius - d_r - r_overlap) <= log_distances) & (log_distances < log_radius + r_overlap)) & ((theta - d_theta - theta_overlap <= E_orientations) & (E_orientations < theta + theta_overlap)) );
            end
        end
    end
      
    % output shape context feature
    SC = histogram;

end

