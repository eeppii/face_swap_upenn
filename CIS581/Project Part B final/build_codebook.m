function [ Codebook, Codebook_delta ] = build_codebook( training_data_objects, training_data_locations)
%script reads in face_cell and face_loc_cell (manually extracted
%eye,mouth.. from movie sequence), both are nfaces x nfeatures+1 (first
%entry is whole face,resp. size)

%generates the codebook for it, namely SC features for easy manually
%extracted feature

% Script saves codebook and codebook_delta

% codebook is nfaces x nfeatures cell with every entry in cell is array of
%lenghth n_sc and contains in first column the shape context features of size (n_theta, n_r)
% and in second colummn the delta =[y,x] of that feature

% Stores features in face_cell and their location in face_loc_cell
%as [x1,y1,width,height]


% Number of objects -> left eye, right eye, nose, mouth
    num_objects = 4;

    %load('face_cell_for_codebook.mat')
    %load('face_loc_cell_for_codebook.mat')

    [nfaces, ~] = size(training_data_objects);

    Codebook = cell(nfaces, num_objects);
    Codebook_delta = zeros(num_objects,2);

    for i = 1:nfaces
        nose_center = training_data_locations{i,4};
        % Take midpoint in bounding box
        nose_center = [nose_center(1)+nose_center(3)/2, nose_center(2)+nose_center(4)/2];
        
        % Extract corner features
        face_img = training_data_objects{i,1};
        
        try
            face_img = rgb2gray(face_img);
        catch
        end
        % cimg = corner_detector(face_img);
        [Gmag, Gdir] = imgradient(face_img);
        % Take top edge features
        BW = edge(face_img, 'canny');
        % apply mask at edges
        cimg = Gmag.*BW;
        
        % spread out edges taken
        [x_feat, y_feat, ~] = anms(cimg, 600);
        % Just take gradient direction converting to radians
        E_orientation = 2*pi *Gdir / 180;

        for j = 1:num_objects % left eye,right eye, nose, mouth
           % Get image
           Img_win = training_data_objects{i,j+1};

           % convert to grayscale
           try
               Img_win = rgb2gray(Img_win);
           catch
           end
           Img_win = double(Img_win);

           % Extract corner features
           %cimg = corner_detector(Img_win);
           % Take up to 10 corner features for objects
           %[x_feat, y_feat, ~] = anms(cimg, 10);

           % Extract shape context features
           window_size = size(Img_win);
           SC = extract_shape_context_features(window_size, training_data_locations{i,j+1}(1) + window_size(2)/2, training_data_locations{i,j+1}(2) + window_size(1)/2, x_feat, y_feat, E_orientation);

           % Make codebook entry
           CE = cell(1,2); % {shape_context_feature, size}
           CE{1,1} = SC;
           CE{1,2} = size(Img_win);

           Codebook{i,j} = CE;

           % Get distance from nose center
           object_center = training_data_locations{i,j+1};
           object_center = [object_center(1)+ object_center(3)/2, object_center(2)+object_center(4)/2];
           Codebook_delta(j,:) = Codebook_delta(j,:) + (object_center - nose_center); %NOT YET DEBUGGED

        end
    end
    % average deltas
    Codebook_delta = round(Codebook_delta/nfaces);

%     save('Codebook_test','Codebook');
%     save('Codebook_delta_test', 'Codebook_delta');
end

