function [ M, Idx ] = get_min_n_elements( V, n )
%GET_TOP_N_ELEMENTS gets the min unique n elements and their indices.
% (INPUT)
%   V is a matrix or vector
%   n is a scalar > 0
% (OUTPUT)
%   M the values
%   Idx the linearized indices in V
    
    M = zeros(n,1);
    Idx = [];
    V = V(:);
    N = size(V,1);
    if n > N
        n = N;
    end
    
    for i = 1:n
        V(Idx) = Inf;
        [M(i),Idx(i)] = min(V);


    end
    Idx = Idx';
end

