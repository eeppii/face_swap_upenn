function morphed_im = morph_tps_wrapper(im1, im1_pts, im2_pts)
%MORPHED IM = MORPH TPS WRAPPER(IM1, IM2, IM1 PTS, IM2 PTS, WARP FRAC, DISSOLVE FRAC) Summary of this function goes here


%get coeff for image1
[a1_x,ax_x,ay_x,w_x] = est_tps(im2_pts, im1_pts(:,1));
[a1_y,ax_y,ay_y,w_y] = est_tps(im2_pts, im1_pts(:,2));

%handle image sizes
Nr=ceil(max(im2_pts(:,2)));
Nc=ceil(max(im2_pts(:,1)));
sz=[Nr,Nc];

%morph images
morph_im1 = morph_tps(im1, a1_x, ax_x, ay_x, w_x, a1_y, ax_y, ay_y, w_y, im2_pts, sz);

%create final image
morphed_im=zeros(sz(1),sz(2),3);
morphed_im=uint8(morphed_im);

morphed_im(:,:,1)=double(morph_im1(:,:,1));
morphed_im(:,:,2)=double(morph_im1(:,:,2));
morphed_im(:,:,3)=double(morph_im1(:,:,3));
   

end

