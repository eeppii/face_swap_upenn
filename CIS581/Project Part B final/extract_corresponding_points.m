function [ P, x,y ] = extract_corresponding_points( CostMaps,  Codebook_delta)
%UNTITLED Summary of this function goes here
% (INPUT)
%   Cost_Maps: (num_objecs, 1) cell array of the cost maps for the target
%       image
%   Codebook_delta: (num_objects, 2) matrix of the vector (x,y) from the nose to the facial
%       feature (1: left eye, 2: right eye, 3: nose, 4: mouth)
% (OUTPUT)
%   P: (num_objects, 2) matrix of the location of the facial feature points
    
    num_objects = size(CostMaps,1);
    
    % Initialize locations
    P = zeros(num_objects, 2);
    Codebook_delta = round(Codebook_delta);
    % offset = (max(round(Codebook_delta)) - min(round(Codebook_delta)));
    %[H, W] = size(CostMaps{1});
    
    % slope for distance transform
    avg_cost = 0;
    for i = 1:num_objects
        avg_cost = avg_cost + mean(CostMaps{i}(CostMaps{i} ~= Inf))/num_objects;
    end
    
    slope = avg_cost / 30;

    for obj = 1:num_objects
        %[H,W] = size(CostMaps{obj});

        % imagesc(CostMaps{obj});
        ObjectLocMap = distance_transform(CostMaps{obj}, slope);
        for i = 1:num_objects
            if i ~= obj
                B = distance_transform(CostMaps{i}, slope);
                delta = Codebook_delta(i,:) - Codebook_delta(obj,:);
                
                % shift right
                if delta(1) < 0
                    % B = B(1:end, abs(delta(1))+1:end);
                    B = B(1:end, 1:end - abs(delta(1)));
                    B = padarray(B, [0, abs(delta(1))], Inf, 'pre');
                % shift left
                else
                    %NoseLocMap = padarray(NoseLocMap, [0, abs(Codebook_delta(i,1))], 'pre');
                    % B = B(1:end, 1:end - abs(delta(1)));
                    B = B(1:end, abs(delta(1))+1:end);
                    B = padarray(B, [0, abs(delta(1))], Inf, 'post');
                end

                if delta(2) < 0
                    %NoseLocMap = padarray(NoseLocMap, [abs(Codebook_delta(i,2)), 0], 'post');
                    % shift down
                    % B = B(abs(delta(2))+1:end, 1:end);
                    B = B(1:end - abs(delta(2)), 1:end);
                    B = padarray(B, [abs(delta(2)), 0], Inf, 'pre');
                else
                    %NoseLocMap = padarray(NoseLocMap, [abs(Codebook_delta(i,2)), 0], 'pre');
                    % B = B(1:end - abs(delta(2)), 1:end);
                    B = B(abs(delta(2))+1:end, 1:end);
                    B = padarray(B, [abs(delta(2)), 0], Inf, 'post');
                end

                % Add
                %imshow(B*2000);
                ObjectLocMap = ObjectLocMap + B/num_objects;
                %imshow(ObjectLocMap*1000);
                
            end
        end

        [~, Idx] = get_min_n_elements(ObjectLocMap, 4);
        %[~, Idx] = min(ObjectLocMap(:));
        [y,x] = ind2sub(size(ObjectLocMap), Idx);
        P(obj,:) = [mean(x),mean(y)];
        % P(obj,:) = [x, y];
        % imagesc(ObjectLocMap.^0.25);
    end
    
end

