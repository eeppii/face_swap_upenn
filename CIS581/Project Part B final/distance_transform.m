function [ D_map ] = distance_transform( CostMap, slope )
%DISTANCE_TRANSFORM Computes the conic distance transform using linear
%distance for a cost map. Uses dynammic programming to find minimum in
%linear time.
% (INPUT)
%   CostMap: (H x W) map of costs of matching objects
%   slope: scalar amount of distance to add in dynamic programming
% (OUTPUT)
%   D_map: (H x W) minimum distance cost map

    [H, W] = size(CostMap);
    D_map = Inf(H,W);
    
    % first pass going in forward direction
    for row = 2:H
        for col = 2:W
            % take minimum of current cost, or previous cost + slope
            D_map(row,col) = min([CostMap(row,col), D_map(row-1,col) + slope, D_map(row,col-1) + slope]);
        end
    end
    
    % Second pass going in opposite direction
    for row = linspace(H-1,1,H-1)
        for col = linspace(W-1,1,W-1)
            D_map(row,col) = min([D_map(row,col), D_map(row+1,col) + slope, D_map(row,col+1) + slope]);
        end
    end
    
    % D_map = exp(-D_map);

end

