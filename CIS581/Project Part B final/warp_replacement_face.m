function [warped_face]=warp_replacement_face(obama,obama_feat_loc,videoFrame_cut, P)
%warps the replacement face
%obama=image of face
%obama_feat_loc=array of size nfeatures x 2 =[xeyel,yeyel;xeyer,yeyer;xnose,ynose;xmouth,ymouth]
%videoFrame_cut is target face
%P is target face features = [xeyel,yeyel;xeyer,yeyer;xnose,ynose;xmouth,ymouth]

% addpath warping_functions

%note: from original code (Proj2) warp_frac=1, dissolve_frac=0 and img2 not
%needed!

%use obama_feat_loc infos
[nr_o,nc_o,~]=size(obama);
[nr_v,nc_v,~]=size(videoFrame_cut);

%add corners of obama
obama_loc= [1 1;...
          nc_o 1;...
          1 nr_o;...
          nc_o nr_o;
          obama_feat_loc];
      
%add corners to target face
target_loc=[1 1;...
          nc_v 1;...
          1 nr_v;...
          nc_v nr_v;
          P];

%warp
warped_face = morph_tps_wrapper(obama, obama_loc, target_loc);

end

