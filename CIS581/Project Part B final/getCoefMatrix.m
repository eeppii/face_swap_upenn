function coefM = getCoefMatrix(indexes)

% (INPUT) indexes: h0w0 matrix representing the indices of each replacement pixel.
% (OUTPUT) coefM: n  n sparse matrix representing the coecient matrix. n is the
% number of replacement pixels.

    n = length(find(indexes));
    
    coefM = sparse(n,n);
    
    % |Np| at fp
    % -1 for fq (neighbors of fp)
    for p = 1:n
        
        Np=4; %default, Np=Np-1 if out of bound
        
        [y,x] = find(indexes == p);

        neighbors = [0,0,0,0];
        if x ~= 1
            neighbors(1) = indexes(y,x-1);
        else
            Np=Np-1;
        end
        if y ~= 1
            neighbors(2) = indexes(y-1,x);
        else
            Np=Np-1;
        end
        if x ~= size(indexes,2)
            neighbors(3) = indexes(y,x+1);
        else
            Np=Np-1;
        end
        if y ~= size(indexes,1)
            neighbors(4) = indexes(y+1,x);
        else
            Np=Np-1;
        end
        
%         Np = nnz(neighbors);
        
        coefM(p,p) = Np;
        coefM(p,neighbors(neighbors ~= 0)) = -1;
    end
    
end
