# face-swap
CIS 581 Final project

# Final report
### Objective

* Build up the whole pipeline for face replacement.
* Focus on the implementation of Shape Context Feature extraction and Face Feature Localization in the target face using Pictorial Structure (Option 1).

### Algorithm

Our algorithm includes the complete pipeline for face replacement, namely includes the following steps:

* Define a replacement face and its feature(object) locations.
* Build a codebook based on a few frames of the test video.
* Retrieve edge features with Canny edge detection and ANMS on target image.
* Extract shape context features (log-radial distance and edge orientation with angle-blur).
* Compare shape context features of target image with Codebook using Chi-Squared distance.
* Use Conic Distance Transform on cost map and shifting for pictorial structure.
* Get facial object locations from minimum response of cost maps.
* Warp replacement face.
* Carve warped replacement face.
* Blend to target image.

### Implementation

In this section the steps of the Section “Algorithm” are discussed in more detail, the visualization is shown in Section “Visualization/Analysis.”

* Define a replacement face and its feature(object) locations.
	In an image of Obama’s face, we selected the locations of the eyes, nose and mouth manually.
* Build a codebook based on a few frames of the test video.
	We chose “easy4” as our test video, as it displayed only one face (see next step). In a few frames, we manually extracted the features in different configurations (e.g. open/closed eye and mouth, nose from the side and front).
	We extracted the shape context features of those for the codebook (Based on Edge Map and ANMS like described in the next step).
	Also, the average distance of the features to the nose are stored for the shifting in a later step.
* Retrieve edge features with Canny edge detection and ANMS on target image.
	The pre-implemented vision.CascadeObjectDetector() in Matlab is run to detect the faces in the target image. We select the biggest detected face as the target face. We did not focus on this part, and choosing between multiple detected target faces in one frame was left for a later implementation.
	Pre-implemented Canny Edge by Matlab is run on the target face. We take the binary edge mask and apply it to the image gradient. We then run ANMS to keep the strongest 600 features (For this we used our code from Project 3).
* Extract shape context features (log-radial distance and edge orientation with angle-blur).
	We set the number of radial bins to be 4 and number of edge bins to be 12.
	For each edge feature, we bin the log distance from the center of the frame. The maximum radius is set to the half the maximum of the frame width and height. Each edge orientation is subtracted from the average angle of the polar coordinates of the edge features with respect to the center frame location. These are then counted in their respective bins.
	Angle blur was implemented with an overlap of delta_angle/6 between the edge orientations so that angles close to the edge of a bin would be counted twice. The blur increased linearly with the radius so that features farther away would have a larger overlap.
* Compare shape context features of target image with Codebook using Chi-Squared distance.
	A sliding window computes a shape context feature every 4 pixels in the target image.
	We compare the target image shape context feature with the features from the codebook using Chi-Squared Distance.
	The minimum distance is chosen and output to the cost map.
* Use Conic Distance Transform on cost map and shifting for pictorial structure.
	We apply Conic Distance, yielding the cost map used for shifting (the slope of the cones we defined as a fraction of the average cost of the map).
	The cost maps for the 4 different features are shifted according to the average feature-to-nose distances of the training data.
* Get facial object locations from minimum response of cost maps.
	The shifted cost maps are summed up, yielding the voting map (The non-intersecting areas are set to infinity).
	The 4 minima of the voting map are averaged, yielding the facial object location in the target face.
* Warp image.
	We implemented warping using the code from project 2.
	Warping based on TPS was used. The TPS is based on the 4 detected feature locations from the pictorial structure voting and the image corners (to constrain the image size).
* Carve warped replacement face.
	We converted the warped replacement face to a log-polar image, using code we found in the internet (see references). However, to convert the feature locations to the log-polar image we had to implement the used formulas on our own.
	For carving in the lop-polar space we used our code from project 4A.
	Our code looks for all possible seams carving around all 4 features, hence does not carve out e.g. an eye. Only carvings containing all 4 features are accepted.
	For the 10 lowest-cost carvings loop-closing is applied. The new lowest-cost carving is used as the final carving.
	The carving is transformed back to Cartesian space.
	Based on this carving, a mask is generated for the blending part.
* Blend image
	We re-used our code from project 4A for blending.
* Generating the Movie
	We filter the feature locations with a low pass filter to get smoother results. Location_k=0.8⋅Location_new+0.2⋅Location_(k-1) Also, if the deformation differs strongly from the dimensions of the training data the replacement face is rejected and the location from the previous step is used (this only occurred in a few frames of the video).


